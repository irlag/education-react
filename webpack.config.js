const webpack = require('webpack');
const path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';

let config = {
    context: path.resolve(__dirname, './src'),
    entry: {
        app: './app',
        vendor: ["jquery"]
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].[hash].js'
    },
    resolve: {
        extensions: ['.jsx', '.js'],
        modules: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules')
        ],
        alias: {
            Components: path.resolve(__dirname, './src/components'),
            Templates: path.resolve(__dirname, './src/templates'),
            Data: path.resolve(__dirname, './src/data'),
            Api: path.resolve(__dirname, './src/api')
        }
    },
    devtool: NODE_ENV == 'development' ? 'source-map' : false,
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader'
                        /*options: {
                            presets: ['es2015', 'react'],
                            plugins: ['transform-runtime']
                        }*/
                    }
                ]
            },
            {
                test: /\.hbs/,
                loader: 'handlebars-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(scss|sass)$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        'css-loader',
                        'sass-loader'
                    ]
                })
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader'
                        }

                    ]
                })
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'images/'
                            //publicPath: '/assets/'
                        }
                    }
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    {
                        loader: 'file-loader'
                    }
                ]
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
             jQuery: 'jquery'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor'
        }),
        new ExtractTextPlugin({
            filename: '[name].[contenthash].css',
            allChunks: true
        }),
        new AssetsPlugin({
            filename: 'assets.json',
            path: path.resolve(__dirname, './dist')
        }),
        new HtmlWebpackPlugin({
            template: './templates/index.hbs',
            //chunks: ['app'],
            title: 'React'
        })
    ],
    node: {
        console: false,
        dgram: 'empty',
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty',
    },
};

if (NODE_ENV == 'production') {
    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                screw_ie8: true,
                warnings: false,
                drop_console: false,
                unsafe: true
            },
            comments: false
        })
    );
} else {
    config.plugins.push(
        new BrowserSyncPlugin({
            // browse to http://localhost:3000/ during development,
            // ./public directory is being served
            host: 'localhost',
            port: 3000,
            proxy: 'localhost:8080',
            open: true
            //server: { baseDir: ['public'] }
        })
    );
}

module.exports = config;
