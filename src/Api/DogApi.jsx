import request from 'request';
import _ from 'lodash';

class DogApi {
    constructor() {
        this.baseUrl = 'https://dog.ceo/api';
        this.allBreeds = {};
    }

    getAllBreeds(callback) {
        if(!_.isEmpty(this.allBreeds))
            return callback(this.allBreeds);

        let url = '/breeds/list/all';

        request({url: this.baseUrl + url, json: true}, (error, response, body) => {
            this.allBreeds = body.message;
            callback(body.message);
        });
    }

    getBreeds(callback) {
        let url = '/breeds/list';

        request({url: this.baseUrl + url, json: true}, (error, response, body) => {
            /*console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.*/

            if (error || response.statusCode !== 200) {
                //return callback(error || {statusCode: response.statusCode});
            }

            callback(body.message);

            //return JSON.parse(body).message;
        });
    }

    getSubBreeds(breed, callback) {
        return callback(this.allBreeds[breed]);
    }

    getRandomPhoto(breed, subBreed, callback) {
        let url = '/breed';

        if(subBreed)
            url += `/${breed}/${subBreed}/images/random`;
        else
            url += `/${breed}/images/random`;

        request({url: this.baseUrl + url, json: true}, (error, response, body) => {
            callback(body.message);
        });
    }
}

export default new DogApi;