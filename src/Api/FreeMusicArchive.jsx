import jsonHttp from 'json-http';

/*
jsonHttp.getJson(jsonUrl, function(err, response) {
    console.log(response);
});
*/

class Fma {
    constructor() {
        this.baseUrl = 'http://freemusicarchive.org';
        this.format = 'json';
    }

    _createQueryUrl(params) {
        let query = '?';

        if(params.limit) {
            query += '&limit='.concat(params.limit);
        }

        return query.concat('&r=json&v=1');
    }

    getAlbums() {
        let url = this.baseUrl + '/api/get/albums.'.concat(this.format);

        return url;
    }
}

export default new Fma;