import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import dogApi from 'Api/DogApi';

const films = require('Data/Netflix.json');

class Hello extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            results: _.clone(films.Search, true),
        };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
          date: new Date()
        });
    }

    render() {
        return (
            <div>
                <h1 className="title">{this.props.title}</h1>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
                <ul>
                    {this.state.results.map((el) => {
                        return (
                            <li key={el.imdbID}>{el.Title}</li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

ReactDOM.render(
    <Hello title='Hello!!!' />,
    document.getElementById('hello')
);