import React from 'react';
import chosen from 'chosen-js'

import 'chosen-js/chosen.css';

class Chosen extends React.Component {
    componentDidMount() {
        this.$el = $(this.el);
        this.$el.chosen({disable_search_threshold: 10});

        this.handleChange = this.handleChange.bind(this);
        this.$el.on('change', this.handleChange);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.children !== this.props.children) {
            this.$el.trigger("chosen:updated");
        }
    }

    componentWillUnmount() {
        this.$el.off('change', this.handleChange);
        this.$el.chosen('destroy');
    }

    handleChange(e) {
        this.props.onChange(e);
    }

    render() {
        return (
            <div>
                <label htmlFor={this.props.id}>{this.props.label}:</label>
                <select className="form-control" id={this.props.id} ref={el => this.el = el}>
                    {this.props.children}
                </select>
            </div>
        );
    }
}

export default Chosen;