import React from 'react';
import ReactDOM from 'react-dom';
import BreedPhoto from 'Components/BreedPhoto';
import Chosen from 'Components/Chosen';
import dogApi from 'Api/DogApi';
import _ from 'lodash';

class Breed extends React.Component {
    constructor() {
        super();

        this.state = {
            breeds: [],
            subBreeds: [],
            currentBreed: '',
            currentSubBreed: ''
        };
    }

    componentDidMount() {
        this.getAllBreeds();
        //this.getBreeds();
    }

    handleChange(e) {
        this.props.onChange(e.target.value);
    }

    getAllBreeds() {
        dogApi.getAllBreeds(this.setAllBreeds.bind(this));

    }

    setAllBreeds(allBreeds) {
        let breeds = _.keys(allBreeds);

        this.setState({
            breeds: breeds,
            subBreeds: allBreeds[breeds[0]],
            currentBreed: breeds[0],
            currentSubBreed: allBreeds[breeds[0]][0]
        });
    }

    getBreeds() {
        dogApi.getBreeds(this.setBreeds.bind(this));
    }

    setBreeds(breeds) {
        this.setState({
            breeds: breeds,
            currentBreed: breeds[0]
        });

        this.getSubBreeds(breeds[0]);
    }

    getSubBreeds() {
        dogApi.getSubBreeds(this.state.currentBreed, this.setSubBreeds.bind(this));
    }

    setSubBreeds(subBreeds) {
        if(subBreeds.length)
            this.setState({
                subBreeds: subBreeds,
                currentSubBreed: subBreeds[0]
            });
        else
            this.setState({
                subBreeds: [],
                currentSubBreed: ''
            });
    }

    onChangeBreed(event) {
        this.setState(
            {
                currentBreed: event.target.value
            },
            this.getSubBreeds
        );
    }

    onChangeSubBreed(event) {
        this.setState({currentSubBreed: event.target.value});
    }

    renderSubBreed() {
        if(this.state.subBreeds.length > 0)
            return (
                <div className="row show-grid">
                    <div className="col-12 col-sm-10 col-sm-offset-1">
                        <div className="form-group">
                            <Chosen id="sub-breed-selector" label="Chouse sub-breed" onChange={this.onChangeSubBreed.bind(this)}>
                                {this.state.subBreeds.map(
                                    (subBreed) => {
                                        return (
                                            <option value={subBreed} key={subBreed}>{subBreed}</option>
                                        );
                                    }
                                )}
                            </Chosen>
                        </div>
                    </div>
                </div>
            );
    }

    renderBreedPhoto() {
        if(this.state.currentBreed)
            return (
                <BreedPhoto
                    currentBreed={this.state.currentBreed}
                    currentSubBreed={this.state.currentSubBreed}
                />
            );
    }

    render() {
        return (
            <div>
                <div className="row show-grid">
                    <div className="col-12 col-sm-10 col-sm-offset-1">
                        <div className="form-group">
                            <Chosen id="breed-selector" label="Chouse breed" onChange={this.onChangeBreed.bind(this)}>
                                {this.state.breeds.map(
                                    (breed) => {
                                        return (
                                            <option value={breed} key={breed}>{breed}</option>
                                        );
                                    }
                                )}
                            </Chosen>
                        </div>
                    </div>
                </div>
                {this.renderSubBreed()}
                {this.renderBreedPhoto()}
            </div>
        );
    }
}

ReactDOM.render(
    <Breed />,
    document.getElementById('breed')
);