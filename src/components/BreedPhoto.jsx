import React from 'react';
import request from 'request';
import dogApi from 'Api/DogApi';

class BreedPhoto extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentBreed: this.props.currentBreed,
            currentSubBreed: this.props.currentSubBreed,
            breedPhotoUrl: ''
        };
    }

    componentDidMount() {
        this.getRandomPhoto();
    }

    componentDidUpdate() {

    }

    componentWillReceiveProps(nextProps) {
        this.setState(
            {
                currentBreed: nextProps.currentBreed,
                currentSubBreed: nextProps.currentSubBreed,
            },
            this.getRandomPhoto
        );
    }

    setBreedPhoto(breedPhotoUrl) {
        this.setState({breedPhotoUrl: breedPhotoUrl});
    }

    getRandomPhoto() {
        if(!this.state.currentBreed)
            return;

        dogApi.getRandomPhoto(this.state.currentBreed, this.state.currentSubBreed,this.setBreedPhoto.bind(this));
    }

    onClick() {
        this.getRandomPhoto();
    }

    render() {
        return (
            <div className="row show-grid">
                <div className="col-12 col-sm-10 col-sm-offset-1">
                    <img className="center-block img-responsive img-rounded" src={this.state.breedPhotoUrl} alt="" title="More angry dogs :)!!!" onClick={this.onClick.bind(this)} />
                    <br />
                    <button type="button" className="center-block btn btn-primary btn-lg" onClick={this.onClick.bind(this)}>More angry dogs :)!!!</button>
                </div>
            </div>
        );
    }
}

export default BreedPhoto;