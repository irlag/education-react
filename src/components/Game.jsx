import React from 'react';
import ReactDOM from 'react-dom';

class Square extends React.Component {
    render() {
        let checkedClass = '';
        let player = '';
        if (this.props.value == 'X') {
            checkedClass = ' fa fa-times';
            player = ' player-1';
        } else if(this.props.value == 'O') {
            checkedClass = ' fa fa-circle-o';
            player = ' player-2';
        }

        return (
            <div className="square">
                <input type="radio" data-key={this.props.itemKey} id={'square-' + this.props.itemKey} onClick={this.props.onClick} />
                <label className={'turn ' + player} htmlFor={'square-' + this.props.itemKey}></label>
            </div>
        );
    }
}

class Board extends React.Component {
    constructor() {
        super();
        this.state = {
            squares: Array(9).fill(null),
            xIsNext: true,
        };
    }

    static calculateWinner(squares) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                return squares[a];
            }
        }
        return null;
    }

    handleClick(e) {
        const squares = this.state.squares.slice();
        const squareKey = e.target.dataset.key;

        if (Board.calculateWinner(squares) || squares[squareKey]) {
            e.preventDefault();
            return;
        }
        squares[squareKey] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            squares: squares,
            xIsNext: !this.state.xIsNext,
        });
    }

    renderSquare(i) {
        return (
            <Square
                value={this.state.squares[i]}
                itemKey={i}
                onClick={(e) => this.handleClick(e)}
            />
        );
    }

    render() {
        const winner = Board.calculateWinner(this.state.squares);
        let status;
        if (winner) {
            status = `Winner: ${winner}`;
        } else {
            status = `Next player: ${(this.state.xIsNext ? 'X' : 'O')}`;
        }

        return (
            <div className="square-wrap">
                {this.renderSquare(0)}
                {this.renderSquare(1)}
                {this.renderSquare(2)}
                {this.renderSquare(3)}
                {this.renderSquare(4)}
                {this.renderSquare(5)}
                {this.renderSquare(6)}
                {this.renderSquare(7)}
                {this.renderSquare(8)}
            </div>
        );
    }
}

class Game extends React.Component {
    render() {
        return (
            <div className="game">
                <div className="game-board">
                    <Board />
                </div>
                <div className="game-info">
                    <div>{/* status */}</div>
                    <ol>{/* TODO */}</ol>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
